package com.omprakash.academiaaccelerator;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by omprakash on 25/9/17.
 */


public class DetailsAdapter extends RecyclerView.Adapter<DetailsAdapter.MyViewHolder> {

    public interface OnRecyclerItemClickListener{
        void onItemClick(Model model);
    }

    List<Model> list;
    Context context;
    private OnRecyclerItemClickListener listener;

    public DetailsAdapter(List<Model> list, Context context,OnRecyclerItemClickListener listener) {
        this.list = list;
        this.listener = listener;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Model m = list.get(position);
        holder.bind(list.get(position),listener);
        holder.name.setText(m.getName());
        holder.email.setText(m.getEmail());
        holder.address.setText(m.getAddress());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView name;
        private TextView email;
        private TextView address;
        public MyViewHolder(View itemView) {
            super(itemView);
            name = (TextView)itemView.findViewById(R.id.name);
            email = (TextView) itemView.findViewById(R.id.email);
            address = itemView.findViewById(R.id.address);
        }

        public void bind(final Model model, final OnRecyclerItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(model);
                }
            });
        }
    }
}
