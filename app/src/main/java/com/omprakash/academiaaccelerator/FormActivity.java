package com.omprakash.academiaaccelerator;

import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class FormActivity extends AppCompatActivity {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference databaseReference;
    Button addModel;
    ArrayList<Model> list = new ArrayList<>();
    RecyclerView recyclerView;
    DetailsAdapter detailsAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_form);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Welcome Home !");
        setSupportActionBar(toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setNestedScrollingEnabled(false);
        addModel = (Button) findViewById(R.id.addModel);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                finish();
            }
        });

        databaseReference = database.getReference();


        databaseReference.child("models").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list = new ArrayList<Model>();
                for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Model model = snapshot.getValue(Model.class);
                    list.add(model);
                }
                detailsAdapter = new DetailsAdapter(list, FormActivity.this, new DetailsAdapter.OnRecyclerItemClickListener() {
                    @Override
                    public void onItemClick(Model model) {

                    }
                });
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(FormActivity.this);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(detailsAdapter);
                detailsAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        addModel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(FormActivity.this,"Test",Toast.LENGTH_SHORT).show();
                final Dialog dialog  = new Dialog(FormActivity.this);
                dialog.setContentView(R.layout.custom_dialog);
                dialog.setTitle("Add to Firebase");

                final EditText etName = (EditText)dialog.findViewById(R.id.etName);
                final EditText etEmail = (EditText)dialog.findViewById(R.id.etEmail);
                final EditText etAddress = (EditText) dialog.findViewById(R.id.etAddress);
                Button addToFirebase = (Button) dialog.findViewById(R.id.addToFirebase);
                Button cancel = (Button)dialog.findViewById(R.id.cancel);

                addToFirebase.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String a = etName.getText().toString();
                        String b = etEmail.getText().toString();
                        String c = etAddress.getText().toString();
                        Model model = new Model(a,b,c);
                        model.setId(databaseReference.child("models").push().getKey());
                        databaseReference.child("models").child(model.getId()).setValue(model);
                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

    }


}
