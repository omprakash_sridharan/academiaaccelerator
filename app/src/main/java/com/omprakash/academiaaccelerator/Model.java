package com.omprakash.academiaaccelerator;

/**
 * Created by omprakash on 25/9/17.
 */

public class Model {
    public String id;
    public String name;
    public String email;
    public String address;

    public Model() {
    }

    public Model(String name, String email, String address) {
        this.name = name;
        this.email = email;
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
